import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Video } from '../../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
})
export class VideoPlayerComponent {
  @Input() set videoId(value: string | null) {
    if (value) {
      const url = `https://www.youtube.com/embed/${value}`;
      this.youtubeSafeUrl =
        this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }

  youtubeSafeUrl: SafeUrl | undefined;

  constructor(private domSanitizer: DomSanitizer) {}
}
