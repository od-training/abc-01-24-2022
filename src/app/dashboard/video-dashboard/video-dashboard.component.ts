import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';

import { Video } from '../../types';

const videoIdQueryParam = 'selectedVideoId';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css'],
})
export class VideoDashboardComponent implements OnInit {
  videoList: Observable<Video[]>;
  selectedVideoId: Observable<string | null>;

  // VideoDataService - DATA LAYER
  // |
  // V
  // VideoDashboardComponent - STATE LAYER & UI LAYER - Parent
  // |
  // V
  // VideoPlayerComponent - UI LAYER (building block - re-usable) - Child

  constructor(
    private vds: VideoDataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    // pipeable operator - tranforming observable values
    // [{}, {}, {}], [{}, {}], [{}, {}, {}, {}]

    // Array.map
    // {}
    // {}

    // arrays & objects are accessed by reference
    this.videoList = vds.getVideos().pipe(
      map((videos) =>
        videos.map((video) => ({
          ...video,
          title: video.title.toUpperCase(),
        }))
      )
    );

    this.selectedVideoId = this.route.queryParamMap.pipe(
      map((params) => params.get(videoIdQueryParam))
      // switchMap(id => this.vds.getVideo(id ?? ''))
    );
  }

  ngOnInit(): void {}

  setSelectedVideo(video: Video) {
    this.router.navigate([], {
      queryParams: {
        [videoIdQueryParam]: video.id,
      },
      queryParamsHandling: 'merge',
    });
  }
}
