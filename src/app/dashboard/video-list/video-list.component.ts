import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css'],
})
export class VideoListComponent implements OnInit {
  // explicit
  @Input() videos: Video[] = [];
  @Input() activeVideoId: string | null = null;

  @Output() videoSelected = new EventEmitter<Video>();

  constructor() {}

  videoClicked(video: Video) {
    this.videoSelected.emit(video);
  }

  ngOnInit(): void {}
}
