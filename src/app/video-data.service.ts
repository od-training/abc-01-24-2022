import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Video } from './types';

const baseUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  getVideos() {
    return this.http.get<Video[]>(`${baseUrl}/videos`);
  }

  getVideo(id: string) {
    return this.http.get<Video>(`${baseUrl}/videos/${id}`);
  }
}
